-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.1.10-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win32
-- HeidiSQL Versión:             9.5.0.5278
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para crafties
CREATE DATABASE IF NOT EXISTS `crafties` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci */;
USE `crafties`;

-- Volcando estructura para tabla crafties.category
CREATE TABLE IF NOT EXISTS `category` (
  `category_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_name` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla crafties.category: ~19 rows (aproximadamente)
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` (`category_id`, `category_name`) VALUES
	(4, 'Cumpleaños'),
	(5, 'Viaje'),
	(6, 'Niños'),
	(7, 'Joyas'),
	(8, 'Fotografía'),
	(9, 'Haloween'),
	(10, 'Día de muertos'),
	(11, 'Festividades'),
	(12, 'Navidad'),
	(13, 'Geek'),
	(14, 'Juegos y Juguet'),
	(15, 'Artículos craft'),
	(16, 'Oficina'),
	(17, 'Regalos'),
	(18, 'Mascotas'),
	(21, 'testing'),
	(22, 'testing 2'),
	(23, 'Probando la re'),
	(24, 'fasfasfasfasf');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;

-- Volcando estructura para tabla crafties.contact
CREATE TABLE IF NOT EXISTS `contact` (
  `contact_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_spanish_ci NOT NULL DEFAULT '0',
  `last_name` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `message` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`contact_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla crafties.contact: ~11 rows (aproximadamente)
/*!40000 ALTER TABLE `contact` DISABLE KEYS */;
INSERT INTO `contact` (`contact_id`, `name`, `last_name`, `email`, `phone`, `message`) VALUES
	(1, 'Gabriel', 'Rosas', 'gabriel.rosas.10gmail.com', '5543832540', 'Probando el formulario de contacto'),
	(2, 'gdfg', 'dfgdfg', 'gabriel.rosas.10@gmail.com', '213421412412', 'hola estamos probando el formulario de contacto'),
	(3, 'fsfesf', 'fse', 'gabriel.rosas.10@gmail.com', 'fsefsefs', 'efsefsefe'),
	(4, 'fsfesf', 'fse', 'gabriel.rosas.10@gmail.com', 'fsefsefs', 'efsefsefe'),
	(5, 'fsfesf', 'fse', 'gabriel.rosas.10@gmail.com', 'fsefsefs', 'efsefsefe'),
	(6, 'fsfesf', 'fse', 'gabriel.rosas.10@gmail.com', 'fsefsefs', 'efsefsefe'),
	(7, 'gabriel', 'ROSAS ELIAS', 'gabriel.rosas.10@gmail.com', '5543832540', 'mensaje de prueba'),
	(8, 'gdfg', 'RODRÍGUEZ', 'gabriel.rosas.10@gmail.com', '213421412412', 'gseg esgeg'),
	(9, 'gdfg', 'RODRÍGUEZ', 'gabriel.rosas.10@gmail.com', '213421412412', 'gseg esgeg'),
	(10, 'fsfesf', 'GARCÍA', 'GRACI2992@GMAIL.COM', '213421412412', 'hrdhrhdrh'),
	(11, 'hthft', 'htfhth', 'GRACI2992@GMAIL.COM', '213421412412', 'hdrhdrhdrh');
/*!40000 ALTER TABLE `contact` ENABLE KEYS */;

-- Volcando estructura para tabla crafties.products
CREATE TABLE IF NOT EXISTS `products` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `genre` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `description` text COLLATE utf8_spanish_ci,
  `category` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_spanish_ci DEFAULT './img/no-product.jpg',
  `cost` decimal(5,2) unsigned DEFAULT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla crafties.products: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` (`product_id`, `title`, `genre`, `description`, `category`, `image`, `cost`) VALUES
	(1, 'Gamebox: Cajita de regalo 90\'s Rock!', 'articulo destacado', 'Regala una Gamebox 90\'s Rock!', 'Articulos craft, Geek, Regalos', 'http://drive.google.com/uc?export=view&id=1hMleAIn5pvc-6hkhsGkDmtSSQe6wCgwY', 0.00),
	(2, 'Mini cocina DIY: Taiyaki & Odango Popin Cookin', 'articulo destacado', 'Cocina en miniatura', 'Juguetes y juegos, Articulos para niños', 'https://i.ytimg.com/vi/Do79COWhwkA/maxresdefault.jpg', 0.00),
	(3, 'Maquina de escribir para notas', 'articulo destacado', 'Si ya estas aburrido de tus pos it\'s de colores. Esto te interesa', 'Articulos craft, Geek', 'http://drive.google.com/uc?export=view&id=1sa10YYNsZ8hUcgCpadzrKORec4hik5ZC', 0.00),
	(4, 'Flor Dahlia. Deco Regalo y Tarjetas de Navidad', 'articulo destacado', 'Decora tu casa como las grandes, con estas flores craft! Paquete de 5pzas', 'Articulos craft,Regalos', 'http://drive.google.com/uc?export=view&id=1HBPxepMRvV0NHg0J93QdTFRTUBeaMLMu', 0.00),
	(5, 'Regalo espacial para el día del padre', 'articulo destacado', 'Camisa y corbata decoratives para el dia del padre. Paquete de 4 pzas', 'regalos', 'http://drive.google.com/uc?export=view&id=1TVQj23EfjmCata1ynmGVtQYku_B1M21i', 0.00),
	(6, 'Pulseras tipo macrame para esta primavera', 'articulo destacado', 'Pulsera tipo macrame en zig-zag. Una unidad', 'Articulos craft, Articulos de temporada', 'http://drive.google.com/uc?export=view&id=1EQ3AWztQG2pAxpZ6e_hSJ8hbGTP2_bc7', 0.00),
	(7, 'Vitral decorativo', 'articulo destacado', 'Vitral decorativo para pared o puerta', 'Decorativos para el hogar', 'http://drive.google.com/uc?export=view&id=1F1UPz0DLOR2ij914eYx8A7ZYI6I0K8ek', 0.00),
	(8, 'Organiza tu escritorio', 'articulo destacado', 'Idea original para organizar y decorar tu escritorio ', 'Organizadores, Geek, oficina', 'http://drive.google.com/uc?export=view&id=1MsY3uFxeKWaqdavijr5pE2kEX9CFrw8j', 0.00),
	(9, 'Organizador de escritorio Acordeon', 'articulo destacado', '¿Tienes muchas cosas en tu escritorio que acomodar? Esta idea te va a encantar', 'Organizadores, Geek, oficina', 'http://drive.google.com/uc?export=view&id=115XAA9rYakcezFY8Ort3vbSTqF74_4K1', 0.00),
	(10, 'Calabazas origami', 'articulo destacado', '12 piezas', 'Articulos craft, Articulos de temporada', 'http://drive.google.com/uc?export=view&id=1psyKQt6020gml1KmfIylMKruYHDUXZKh', 0.00),
	(11, 'Libreta creaft 100 hojas', 'articulo destacado', '1 pza. Decora tu libreta como tu quieras', 'regalos, Oficina, Organizadores', 'http://drive.google.com/uc?export=view&id=1FSzjMiBSdHoFbxRnDQxSajv5V67q9hFT', 0.00),
	(12, 'Paquete e 100 bolsas craft personalizables', 'articulo destacado', '1pza. Bolsas craft perzonalizables', 'Regalos, Craft, Oficina', 'http://drive.google.com/uc?export=view&id=1u0n2lwXDhCSBy6Qu65U-2dEjqxEFALih', 0.00),
	(13, 'Amohada de Cámara', 'articulo destacado', '1pza. Decora tu habitación de la mejor manera creativa', 'Hogar, Niños, Fotografía, Craft', 'http://drive.google.com/uc?export=view&id=1PLtXhX4S7EgwhyWMA-xdW3poGpWeW_AF', 0.00),
	(14, 'Almohada dona', 'articulo destacado', '1pza. Decora tu habitación de la mejor manera creativa', 'Hogar, Niños, Craft', 'http://drive.google.com/uc?export=view&id=1hp8tgzVZrif75Dmkw9k3mkSrql1oIHpJ', 0.00),
	(15, 'Almohada perruna', 'articulo destacado', '1pza. Tenemos para ti la major almohada perruna', 'Hogar, Niños, Craft,Mascotas', 'http://drive.google.com/uc?export=view&id=1dq0VfWn8X5oNGpopiLGkbXyFH38ddhlr', 0.00),
	(16, 'Alfombra Grecas', 'articulo destacado', '1pza, Personaliza tu hogar', 'Hogar', 'http://drive.google.com/uc?export=view&id=1sHPz1XiRz1CusF3NY6IsD_zALNQ4-If-', 0.00),
	(17, 'Luces decorativas', 'articulo destacado', '1pza. Luces con 100 focos de colores led', 'Hogar', 'http://drive.google.com/uc?export=view&id=1e955HDwfUtwb1saSoGnyGKVf3eWYKH0h', 0.00),
	(18, 'Cortina de baño impresa', 'articulo destacado', '1pza. Cortina para baño perzonalizable', 'Hogar, regalos', 'http://drive.google.com/uc?export=view&id=1R_tOqDYIzHM3CHjrzcDaBmj8y1q5Y50n', 0.00),
	(19, 'Taburetes creaft', 'articulo destacado', 'Taburete de materiales 100% biodegradables', 'Oficina, Hogar, articulos craft', 'http://drive.google.com/uc?export=view&id=1EXZOgtlq_chzSnO4aWylo1iQoMkMfK87', 0.00),
	(20, 'Viniles decorativo para cocina', 'articulo destacado', 'Diseña tu mozaico para tu cocina. Decorativos prácticos para cocina (precio caculado por metro cuadrado)', 'Hogar', 'http://drive.google.com/uc?export=view&id=1oeVZKL-GNhpGWrk6X2p7h0cGuI1EuZlt', 0.00);
/*!40000 ALTER TABLE `products` ENABLE KEYS */;

-- Volcando estructura para tabla crafties.user
CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_spanish_ci NOT NULL DEFAULT '0',
  `last_name` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `user` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `pass` varchar(32) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci ROW_FORMAT=COMPACT;

-- Volcando datos para la tabla crafties.user: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`user_id`, `name`, `last_name`, `email`, `user`, `pass`) VALUES
	(1, 'gabriel', 'rosas', 'gabriel.rosas.10@gmail.com', 'gabo', '827ccb0eea8a706c4c34a16891f84e7b');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
