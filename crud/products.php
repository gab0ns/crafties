<?php session_start(); ?>
<!DOCTYPE html>
  <html>
    <head>
      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="../css/materialize.min.css"  media="screen,projection"/>
      <link rel="stylesheet" type="text/css" href="../css/styles.css">
      <link rel="stylesheet" type="text/css" href="../css/crud.css">
      <link href="https://fonts.googleapis.com/css?family=Sacramento" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Montserrat|Sacramento" rel="stylesheet">

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <meta charset="utf-8">
    </head>

    <body>
      <?php if(isset($_SESSION['sess_craftie'])): ?>
      <div class="container">
        <!-- Dropdown Structure -->
        <ul id="dropdown1" class="dropdown-content categories">

        </ul>
        <nav>
          <div class="nav-wrapper">
            <a href="../index.php" class="brand-logo"><h3 class="logo">Crafties</h3></a>
            <ul class="right hide-on-med-and-down">
              <li><a href="../index.php">Inicio</a></li>
              <?php if(isset($_SESSION['sess_craftie'])): ?>
                <li><a href="index.php">Categorias</a></li>
                <li><a href="products.php">Productos</a></li>
              <?php endif; ?>
              <?php if(isset($_SESSION['sess_craftie'])): ?>
                <li><a href="../login.php">Cerrar sesión</a></li>
              <?php else: ?>
                <li><a href="../login.php">Iniciar sesión</a></li>
              <?php endif; ?>
              
            </ul>
          </div>
        </nav>

        <div class="row">
          <div class="col s12">
          <hr>      
            <h2>Productos <a href="#add" class="u-btn  u-add"><i class="material-icons prefix">add</i></a></h2>
          </div>  
          <div class="col s12">
            <div class="CRUD">
            <aside class="CRUD-response">
              <div class="u-preload  u-hidden">
                <div class="spinner">
                  <div class="rect1"></div>
                  <div class="rect2"></div>
                  <div class="rect3"></div>
                  <div class="rect4"></div>
                  <div class="rect5"></div>
                </div>
              </div>
              <div class="u-message  u-hidden">
                Mensaje del Servidor
              </div>
            </aside>
            <table class="CRUD-table  u-hidden">
              <tr>
                <th>ID</th>
                <th>Producto</th>
                <th></th>
                <th></th>
              </tr>
            </table>
          </div>
          <section id="add" class="ModalWindow">
            <div class="ModalWindow-box">
              <a href="#"><i class="material-icons prefix">close</i></a>
              <h5>Agregar Producto</h5>
              <form class="Form-add">
                <div class="input-field">
                    <label>Titulo</label>
                    <input type="text" name="title" required id="">
                </div>                
                <div class="input-field">
                    <label>Género</label>
                    <input type="text" name="genre" required id="">
                </div>                
                <div class="input-field">
                    <label>Descripción</label>
                    <input type="text" name="description" required id="">
                </div>                
                <div class="input-field">
                    <label>Categorias</label>
                    <input type="text" name="category" required id="">
                </div>                
                <div class="input-field">
                    <label>Imagen</label>
                    <input type="text" name="image" required id="">
                </div>                
                <div class="input-field">
                    <label>Costo</label>
                    <input type="text" name="cost" required id="">
                </div>                
                <div class="input-field">
                    <input type="submit" class="waves-effect waves-light btn">
                </div>                
              </form>
            </div>
          </section>
          <section id="edit" class="ModalWindow">
            <div class="ModalWindow-box">
              <a href="#"><i class="material-icons prefix">close</i></a>
              <h5>Editar Producto</h5>
              <form class="Form-edit">
                <div class="input-field">
                    <label>Titulo</label>
                    <input type="text" name="title" required id="">
                    <input type="hidden" name="product_id">
                </div>                
                <div class="input-field">
                    <label>Género</label>
                    <input type="text" name="genre" required id="">
                </div>                
                <div class="input-field">
                    <label>Descripción</label>
                    <input type="text" name="description" required id="">
                </div>                
                <div class="input-field">
                    <label>Categorias</label>
                    <input type="text" name="category" required id="">
                </div>                
                <div class="input-field">
                    <label>Imagen</label>
                    <input type="text" name="image" required id="">
                </div>                
                <div class="input-field">
                    <label>Costo</label>
                    <input type="text" name="cost" required id="">
                </div>                
                <div class="input-field">
                    <input type="submit" class="waves-effect waves-light btn">
                </div>  
              </form>
            </div>
          </section>
          <section id="delete" class="ModalWindow">
            <div class="ModalWindow-box">
              <a href="#"><i class="material-icons prefix">close</i></a>
              <h5>Eliminar Producto</h5>
              <form class="Form-delete">
                <p>¿Estás seguro de eliminar el registro <mark></mark>?</p>
                <input type="hidden" name="product_id">
                <input type="submit" name="yes" value="SI"  class="waves-effect waves-light btn">
                <input type="reset" name="no" value="NO"  class="waves-effect waves-light btn">
              </form>
            </div>
          </section>
          <template id="table-row">
            <tr>
              <td class="product_id"></td>
              <td class="product_name"></td>
              <td>
                  <input type="hidden" class="product_genre"  />
                  <input type="hidden" class="product_description"  />
                  <input type="hidden" class="product_category"  />
                  <input type="hidden" class="product_image"  />
                  <input type="hidden" class="product_cost"  />
                <a href="#edit" class="u-btn  u-edit">editar</a>
              </td>
              <td>
                <a href="#delete" class="u-btn  u-delete">eliminar</a>
              </td>
            </tr>
          </template>
          </div>  
        </div>
      </div>
      <div class="row">
        <div class="col s12">
          <footer class="page-footer">
            <div class="container">
              <div class="row">
                <div class="col l12 s12">
                  <hr>
                  <h3 class="footer">Crafties</h3>
                </div>  
              </div>
              </div>
              <div class="footer-copyright">
                <div class="container">
                © 2018 Copyright Crafties
                </div>
              </div>
            </footer>
          </div>
      </div>
      <!--Import jQuery before materialize.js-->
      <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
      <script type="text/javascript" src="../js/materialize.min.js"></script>
      <script type="text/javascript" src="../js/products.js"></script>
      <?php else: ?>
      <?php 
            header("Location: ../login.php");
            exit; 
      ?>
      <?php endif; ?>
    </body>
  </html>