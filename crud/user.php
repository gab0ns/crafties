<?php
//echo "Probando servidor";
//error_reporting(0);
//http://php.net/manual/es/class.mysqli.php
session_start();
$mysql = new mysqli( 'localhost', 'root', 'gab0ns5789', 'crafties' );
$mysql->set_charset('utf8');
if ( $mysql->connect_error ) {
  $res = array(
    'err' => true,
    'type' => 'Error al conectarse a la base de datos',
    'status' => $mysql->connect_errno,
    'statusText' => $mysql->connect_error
  );
} else {
  /* $res = array(
    'err' => false,
    'type' => 'Conexión exitosa a la base de datos'
  ); */
  $action = 'read';
  if ( isset( $_GET['action'] ) ) {
    $action = $_GET['action'];
  }
  switch ($action) {
    case 'create':
      $name = $_POST['name'];
      $last_name = $_POST['last_name'];
      $email = $_POST['email'];
      $phone = $_POST['phone'];
      $message = $_POST['message'];
      $sql = "INSERT INTO user (name, last_name, email, phone, message) VALUES ('$name','$last_name','$email','$phone','$message')";
      $result = $mysql->query($sql);
      if ( $result ) {
        $err = false;
        $data = 'Registro agregado con éxito';
      } else {
        $err = true;
        $data = 'Error al tratar de insertar registro';
      }
      $res = array(
        'err' => $err,
        'type' => 'Acción Create',
        'data' => $data,
        'sql' => $sql
      );  

      break;
    case 'read':
      $user = $_POST['user'];
      $pass = md5($_POST['pass']);
      $sql = "SELECT * FROM user WHERE user = '$user' AND pass = '$pass';";
      $result = $mysql->query($sql);
      $data = array();
      while ( $row = $result->fetch_assoc() ) {
        $_SESSION["sess_craftie"]['name'] = $row['name'];
        $_SESSION["sess_craftie"]['last_name'] = $row['name'];
        $_SESSION["sess_craftie"]['email'] = $row['email'];
      }
      $res = array(
        'err' => false,
        'type' => 'Acción Read',
        'numRows' => $result->num_rows
      );

      break;
    case 'update':
      $category_id = $_POST['category_id'];
      $category_name = $_POST['category_name'];
      $sql = "UPDATE contact SET category_name = '$category_name' WHERE category_id = '$category_id'";
      $result = $mysql->query($sql);
      if ( $result ) {
        $err = false;
        $data = 'Registro actualizado con éxito';
      } else {
        $err = true;
        $data = 'Error al tratar de actualizar el registro';
      }
      $res = array(
        'err' => $err,
        'type' => 'Acción Update',
        'data' => $data,
        'sql' => $sql
      );
      break;
    case 'delete':
      $category_id = $_POST['category_id'];
      $sql = "DELETE FROM category WHERE category_id = $category_id";
      $result = $mysql->query($sql);
      if ( $result ) {
        $err = false;
        $data = 'Registro eliminado con éxito';
      } else {
        $err = true;
        $data = 'Error al tratar de eliminar el registro';
      }
      $res = array(
        'err' => $err,
        'type' => 'Acción Delete',
        'data' => $data,
        'sql' => $sql
      );
      break;
    default:
      $res = array(
        'err' => true,
        'type' => 'Acción No permitida'
      );
      break;
  }
}
//$result->free();
$mysql->close();
header( 'Content-type: application/json' );
echo json_encode($res);