<?php session_start(); ?>
<!DOCTYPE html>
  <html>
    <head>
      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
      <link rel="stylesheet" type="text/css" href="css/styles.css">
      <link href="https://fonts.googleapis.com/css?family=Sacramento" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Montserrat|Sacramento" rel="stylesheet">

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <meta charset="utf-8">
    </head>

    <body>
      <div class="container">
        <!-- Dropdown Structure -->
        <ul id="dropdown1" class="dropdown-content categories">

        </ul>
        <nav>
          <div class="nav-wrapper">
            <a href="index.php" class="brand-logo"><h3 class="logo">Crafties</h3></a>
            <ul class="right hide-on-med-and-down">
              <?php if(isset($_SESSION['sess_craftie'])): ?>
                <li><a href="crud/index.php">Administración</a></li>
              <?php endif; ?>
              <li><a href="form.php">Contacto</a></li>
              <!-- Dropdown Trigger -->
              <li><a class="dropdown-button" href="#!" data-activates="dropdown1">Categorías<i class="material-icons right">arrow_drop_down</i></a></li>
              <?php if(isset($_SESSION['sess_craftie'])): ?>
                <li><a href="login.php">Cerrar sesión</a></li>
              <?php else: ?>
                <li><a href="login.php">Iniciar sesión</a></li>
              <?php endif; ?>
              
            </ul>
          </div>
        </nav>

        <div class="row">
          <div class="col s12">
          <hr>      
            <h2>Articulos Destacados</h2>
          </div>  
          <div class="col s12 products">
            
          </div>  
        </div>
    </div>  
    <div class="row">
      <div class="col s12">
        <footer class="page-footer">
          <div class="container">
            <div class="row">
              <div class="col l12 s12">
                <hr>
                <h3 class="footer">Crafties</h3>
              </div>  
            </div>
            </div>
            <div class="footer-copyright">
              <div class="container">
              © 2018 Copyright Crafties
              </div>
            </div>
          </footer>
        </div>
    </div>
      <!--Import jQuery before materialize.js-->
      <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
      <script type="text/javascript" src="js/materialize.min.js"></script>
      <script type="text/javascript" src="js/script.js"></script>
    </body>
  </html>