<?php session_start(); ?>
<!DOCTYPE html>
  <html>
    <head>
      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
      <link rel="stylesheet" type="text/css" href="css/styles.css">
      <link href="https://fonts.googleapis.com/css?family=Sacramento" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Montserrat|Sacramento" rel="stylesheet">

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <meta charset="utf-8">
    </head>

    <body>
      <div class="container">
        <!-- Dropdown Structure -->
        <ul id="dropdown1" class="dropdown-content categories">

        </ul>
        <nav>
          <div class="nav-wrapper">
            <a href="index.php" class="brand-logo"><h3 class="logo">Crafties</h3></a>
            <ul class="right hide-on-med-and-down">
              <li><a href="form.php">Contacto</a></li>
              <!-- Dropdown Trigger -->
              <li><a class="dropdown-button" href="#!" data-activates="dropdown1">Categorías<i class="material-icons right">arrow_drop_down</i></a></li>
              <?php if(isset($_SESSION['sess_craftie'])): ?>
                <li><a href="login.php">Cerrar sesión</a></li>
              <?php else: ?>
                <li><a href="login.php">Iniciar sesión</a></li>
              <?php endif; ?>
              
            </ul>
          </div>
        </nav>

        <div class="row">
          <div class="col s12">
          <hr>      
            <h2>Contacto</h2>
          </div>  
          <div class="col s12">
            <article class="col s6 offset-s3">
                <form method="POST" action="crud/form.php?action=create" id="form-contact">
                        <div class="input-field">
                                <i class="material-icons prefix">perm_identity</i>
                                <label for="nombre">Nombre</label>
                                <input type="text" name="name" required>
                        </div>

                        <div class="input-field">
                                <i class="material-icons prefix">person_pin</i>
                                <label for="apellido">Apellido</label>
                                <input type="text" name="last_name" required>
                        </div>

                        <div class="input-field">
                                <i class="material-icons prefix">email</i>
                                <label for="email">Correo</label>
                                <input type="email" name="email" required>
                        </div>
                    
                        <div class="input-field">
                                <i class="material-icons prefix">phone</i>
                                <label for="email">Teléfono</label>
                                <input type="text" name="phone">
                        </div>

                        <div class="input-field">
                                <i class="material-icons prefix">mode_edit</i>
                                <label for="mensaje">Mensaje</label>
                                <textarea name="message" id="" rows="10" class="materialize-textarea"  length="140" required></textarea>
                        </div>

                        <p class="center-align">
                            <button class="waves-effect waves-light btn" type="submit"><i class="material-icons right">send</i>enviar</button>
                        </p>

                </form>
            </article>
          </div>
        </div>  
      </div>  
    <div class="row">
      <div class="col s12">
        <footer class="page-footer">
          <div class="container">
            <div class="row">
              <div class="col l12 s12">
                <hr>
                <h3 class="footer">Crafties</h3>
              </div>  
            </div>
            </div>
            <div class="footer-copyright">
              <div class="container">
              © 2018 Copyright Crafties
              </div>
            </div>
          </footer>
        </div>
    </div>






      <!--Import jQuery before materialize.js-->
      <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
      <script type="text/javascript" src="js/materialize.min.js"></script>
      <script type="text/javascript" src="js/script.js"></script>
    </body>
  </html>