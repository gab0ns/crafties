((d, w, c, j, xhr) => {
  const preload = d.querySelector('.u-preload'),
    message = d.querySelector('.u-message'),
    table = d.querySelector('.CRUD-table'),
    tr = d.getElementById('table-row').content

  function ajaxPOST(req) {
    preload.classList.add('is-active')

    xhr.open(req.method, req.url, true)

    xhr.addEventListener('readystatechange', e => {
      if (xhr.readyState === 4) {

        w.location.hash = '#'
        preload.classList.remove('is-active')
        message.classList.add('is-active')

        if (xhr.status >= 200 && xhr.status < 400) {
          let res = j.parse(xhr.response)
          message.innerHTML = res.data
        } else {
          message.innerHTML = `<b>El servidor NO responde. Error N° ${xhr.status}: <mark>${xhr.statusText}</mark></b>`
        }
        //setTimeout(() => w.location.reload(), 1500)
      }
    })

    xhr.send(req.data)
  }

  d.addEventListener('DOMContentLoaded', e => {
      console.log('carga tabla');
    preload.classList.add('is-active')

    xhr.open('GET', './products_crud.php', true)

    xhr.addEventListener('readystatechange', e => {
      if (xhr.readyState === 4) {
        preload.classList.remove('is-active')
        message.classList.add('is-active')
        //c(xhr)

        if (xhr.status >= 200 && xhr.status < 400) {
          let res = j.parse(xhr.response)
          //c(res)
          if (res.numRows === 0) {
            message.innerHTML = 'No existen datos para la consulta ejecutada'
          } else {
            message.classList.remove('is-active')
            table.classList.add('is-active')

            res.data.forEach(row => {
              //c(row)
                tr.querySelector('.product_id').textContent = row.product_id
                tr.querySelector('.product_name').textContent = row.title
                
                tr.querySelector('.u-edit').dataset.product_id = row.product_id
                tr.querySelector('.u-edit').dataset.title = row.title
                tr.querySelector('.u-edit').dataset.genre = row.genre
                tr.querySelector('.u-edit').dataset.description = row.description
                tr.querySelector('.u-edit').dataset.category = row.category
                tr.querySelector('.u-edit').dataset.image = row.image
                tr.querySelector('.u-edit').dataset.cost = row.cost
                
                tr.querySelector('.u-delete').dataset.product_id = row.product_id
                tr.querySelector('.u-delete').dataset.title = row.title

                let clone = d.importNode(tr, true)
                table.appendChild(clone)
              });
            }
          } else {
            message.innerHTML = `<b>El servidor NO responde. Error N° ${xhr.status}: <mark>${xhr.statusText}</mark></b>`
          }
        }
      })

      xhr.send()
    })

    d.addEventListener('submit', e => {
      if (e.target.matches('form')) {
        let action
        e.preventDefault()

        if (e.target.matches('.Form-add')) {
          action = 'create'
        } else if (e.target.matches('.Form-edit')) {
          action = 'update'
      } else if (e.target.matches('.Form-delete')) {
        action = 'delete'
      } else {
        action = 'not-action'
      }

      ajaxPOST({
        method: 'POST',
        url: `./products_crud.php?action=${action}`,
        data: new FormData(e.target)
      })
    }
  })

  d.addEventListener('click', e => {
    if (e.target.matches('.u-edit')) {
      let form = d.querySelector('.Form-edit')
      c(e.target.dataset)
      //console.log(e.target.dataset);
      form.querySelector('[name="title"]').value = e.target.dataset.title
      form.querySelector('[name="genre"]').value = e.target.dataset.genre
      form.querySelector('[name="description"]').value = e.target.dataset.description
      form.querySelector('[name="category"]').value = e.target.dataset.category
      form.querySelector('[name="image"]').value = e.target.dataset.image
      form.querySelector('[name="cost"]').value = e.target.dataset.cost
      form.querySelector('[name="product_id"]').value = e.target.dataset.product_id
      
    } else if (e.target.matches('.u-delete')) {
      let form = d.querySelector('.Form-delete')
      form.querySelector('[name="product_id"]').value = e.target.dataset.product_id
      form.querySelector('mark').textContent = e.target.dataset.title
    } else if (e.target.matches('[type="reset"')) {
      let form = d.querySelector('.Form-delete')
      form.reset()
      w.location.hash = '#'
    }
  })
})(document, window, console.log, JSON, new XMLHttpRequest());