$(function() {
    
    //peticion para traer los productos
    $.get("crud/products_crud.php?action=read", function(response){
        //recorre las categorias
        var html = '';
        $.each(response.data,function(i,k){
            html += '<div class="col s12 col m6 col xl4">';
            html += '<div class="card">';
            html += '<div class="card-image waves-effect waves-block waves-light">';
            html += '<img class="activator" src="'+k.image+'">';
            html += '</div>';
            html += '<div class="card-content">';
            html += '<span class="card-title activator grey-text text-darken-4">'+k.title+'<i class="material-icons right">more_vert</i></span>';
            html += '<p><a href="#">'+k.category+'</a></p>';
            html += '</div>';
            html += '<div class="card-reveal">';
            html += '<span class="card-title grey-text text-darken-4"><i class="material-icons right">close</i>'+k.title+'</span>';
            html += '<p>'+k.description+'</p>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
        });
        $('.products').html(html);
    });
    
    //peticion para traer las categorias
    $.get("crud/crud.php?action=read", function(response){
        //recorre las categorias
        var html = '';
        $.each(response.data,function(i,k){
            html += '<li><a href="#!">'+k.category_name+'</a></li>';
        });
        $('.categories').html(html);
    });
    
    //envia formulario de contacto
    $('#form-contact').on('submit', function(e){
        e.preventDefault();
        $.post($(this).attr('action'), $(this).serialize(), function(response){
            alert(response.data);
        });
    });
    
    //envia formulario de login
    $('#form-login').on('submit', function(e){
        e.preventDefault();
        $.post($(this).attr('action'), $(this).serialize(), function(response){
            if(response.numRows == 0)
            {
                alert('Usuario y/o contraseña incorrecta.');
            }
            else 
            {
                alert('Sesión iniciada de forma correcta.');
                window.location = 'index.php';
            }
        });
    });
    
});